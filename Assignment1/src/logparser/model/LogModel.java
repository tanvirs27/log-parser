package logparser.model;

import logparser.service.Reader;

import java.util.ArrayList;

/**
 * Created by shahriar on 2/4/18.
 */
public class LogModel {
    private ArrayList<Log> allLogs;
    private Reader reader;

    public LogModel(String fileName) {
        allLogs = new ArrayList<Log>();
        reader = new Reader(fileName);

        allLogs = reader.read();
    }

    public ArrayList<Log> getLogData() {
        return allLogs;
    }
}
