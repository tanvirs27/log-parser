package logparser.model;

import java.io.Serializable;

/**
 * Created by shahriar on 2/1/18.
 */
public class Log implements Serializable {

    private static final long serialVersionUID = 1L;

    private int time;
    private boolean isGET;
    private boolean isPOST;
    private long responseTime;
    private String URI;

    public void setTime(int time) {
        this.time = time;
    }

    public void setIsGET(Boolean isGET) {
        this.isGET = isGET;
    }

    public void setIsPOST(Boolean isPOST) {
        this.isPOST = isPOST;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public int getTime() {
        return time;
    }

    public Boolean getIsGET() {
        return isGET;
    }

    public Boolean getIsPOST() {
        return isPOST;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public String getURI() {
        return URI;
    }

    public String toString() {
        return time + " " + isGET + " " + isPOST + " " + responseTime + " " + URI;
    }
}
