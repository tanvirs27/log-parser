package logparser.view;

import logparser.model.Log;
import logparser.model.SummaryLog;

import java.util.ArrayList;

/**
 * Created by shahriar on 2/4/18.
 */
public class LogView {

    private final static String TIME="Time";
    private final static String GET="GET/POST Count";
    private final static String URI="Total Unique URI Count";
    private final static String RESPONSE_TIME="Total Response Time";

    public void printModel(ArrayList<Log> allLogs) {
        for (Log log : allLogs) {
            System.out.println(log);
        }
    }

    public void printSummary(ArrayList<SummaryLog> summary) {
        System.out.printf("%21s | %14s | %22s | %19s\n", TIME, GET, URI, RESPONSE_TIME);
        for (SummaryLog summaryLog : summary) {
            System.out.println(summaryLog);
        }
    }
}
